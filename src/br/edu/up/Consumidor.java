package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Consumidor {

  public static void main(String[] args) throws NamingException, JMSException {

    // 1. Cria��o de contexto;
    Context ctx = new InitialContext();

    // 2. Busca da fila;
    Queue fila = (Queue) ctx.lookup("jms/FILA_DE_EXEMPLO");
   
    // 3. Busca da f�brica de conex�es;
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");

    // 4. Cria��o da conex�o;
    try(Connection conexao = cf.createConnection("up", "positivo")){
      
      // 5. Inicio a conex�o;
      conexao.start();
      
      // 6. Cria��o da sess�o;
      Session session = conexao.createSession(false, Session.AUTO_ACKNOWLEDGE);
      
      // 7. Cria��o do consumidor;
      MessageConsumer consumidor = session.createConsumer(fila);
      String msg;
      do {
        
        // 8. Leitura das TextMessages;
        TextMessage txtMsg = (TextMessage) consumidor.receive();
        msg = txtMsg.getText();
        System.out.println(msg);
        
      } while(!msg.equals(""));
      
      // 9. Encerramento do consumidor e da conex�o;
      consumidor.close();
      conexao.close();
      
    } finally{
      ctx.close();
      System.out.println("Consumidor encerrado!");
    }
  }
}